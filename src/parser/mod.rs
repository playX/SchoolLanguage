pub mod ast;

pub mod grammar;

use self::grammar::{ExprParser, ProgramParser};
use self::ast::{Expr, Stmt};
use lalrpop_util::ParseError as LParseError;
use lexer::token::Token;
use lexer::{Lexer, LexicalError, Location};

pub type ParseError<'input> = LParseError<Location, Token<'input>, LexicalError>;

pub fn parse_program(input: &str) -> Result<Vec<Stmt>, ParseError> {
    let lexer = Lexer::new(input);
    let parser = ProgramParser::new();
    parser.parse(lexer)
}

pub fn parse_expr(input: &str) -> Result<Expr, ParseError> {
    let lexer = Lexer::new(input);
    let parser = ExprParser::new();
    parser.parse(lexer)
}
