#![allow(unused_imports)]
#![allow(dead_code)]
extern crate school_vm;
#[macro_use]extern crate structopt;
extern crate rustyline;
extern crate lalrpop_util;
#[macro_use]extern crate failure;
#[macro_use]extern crate hmap;
mod parser;
mod lexer;
use school_vm::{vm::bytecode::*,vm::frame::*,vm::VM};

use parser::ParseError;
use parser::{parse_expr,parse_program};
use parser::ast::*;
use lexer::*;

fn stmt(input: &str)  -> Vec<Stmt> {
    let lexer = Lexer::new(input);
    let parser = parser::grammar::ProgramParser::new();
    return parser.parse(lexer).unwrap();
}

use rustyline::Editor;
use std::fs::File;
use std::io::prelude::*;
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
struct Options {
    #[structopt(name = "FILE", parse(from_os_str))]
    pub file: Option<PathBuf>,
    #[structopt(short = "d", long = "debug")]
    pub debug: bool,
}

pub static mut DEBUG: bool = false;

fn main() {

    let mut src = String::new();

    let ops = Options::from_args();
    if let Some(path) = ops.file {
        File::open(path).unwrap().read_to_string(&mut src).unwrap();
    } else {
        panic!("You should enter file path");
    }

    compile(src,ops.debug);

}


pub fn compile(src:String,d: bool) {
    unsafe {DEBUG = d};
    let stmts = stmt(&src);
    let mut cmpl = Compiler::new();
    cmpl.emit(Opcode::PUSH(Value::RustFun(print)));
    cmpl.emit(Opcode::STORE_FAST("print".to_string()));
    for stmt in stmts.iter() {
        cmpl.stmt(stmt);
    }


    //cmpl.emit(Opcode::EXIT);
    let code = cmpl.finalize();
    if d {
        for op in code.iter() {
            println!("{:?}", op);
        }
    }
    let mut vm = VM::new();


    let frame = Frame::new(code,HashMap::new());
    vm.run_frame(frame).unwrap();
}

use std::collections::HashMap;

#[derive(Debug)]
pub struct Compiler {
    pub code: Vec<UOP>,
    pub labels: HashMap<String,Option<usize>>,
}

#[derive(Debug)]
pub enum UOP {
    Op(Opcode),
    Goto(String),
}

use std::fmt;



fn print(_: &mut VM,args: Vec<Value>) -> Option<Value> {
    for a in args.iter() {
        print!("{}",a);
    }
    print!("\n");
    if args.first().is_some() {
        return Some(args.first().unwrap().clone());
    } else {
        return None;
    }
}



impl Compiler {
    pub fn new() -> Self {

        Compiler {
            code: vec![],
            labels: hmap!(),
        }
    }

    pub fn finalize(&mut self) -> Vec<Opcode> {


        self.code
            .iter()
            .map(|e| match e {
                UOP::Goto(ref label) => {
                    Opcode::JUMP(self.labels.get(label).unwrap().unwrap())
                }
                UOP::Op(ref o) => o.clone(),
            })
            .collect()

    }

    pub fn new_label_here(&mut self,s: String) {
        self.labels.insert(s,Some(self.code.len()));
    }

    pub fn new_empty_label(&mut self) -> String {
        let lab_name = self.labels.len().to_string();
        self.labels.insert(lab_name.clone(), None);
        lab_name
    }

    pub fn label_here(&mut self, label: String) {
        *self.labels.get_mut(&label).unwrap() = Some(self.code.len());
    }

    pub fn emit(&mut self,op: Opcode) {
        self.code.push(UOP::Op(op));
    }




    pub fn emit_goto(&mut self,s: String) {
        self.code.push(UOP::Goto(s));
    }

    pub fn stmt(&mut self, stmt: &Stmt) {
        match stmt {
            Stmt::Declaration(ref name,ref expr) => {
                self.expr(expr.clone());
                self.emit(Opcode::STORE_FAST(name.clone()));
            }
            Stmt::ExprStmt(expr) => self.expr(expr.clone()),
            Stmt::Return(expr) => {
                self.expr(expr.clone());
                self.emit(Opcode::RETURN);
            }
            Stmt::If(cond,ift,iff) => {
                self.expr(cond.clone());


                let if_true = self.new_empty_label();
                let if_false = self.new_empty_label();
                let end = self.new_empty_label();
                let iif = self.new_empty_label();
                self.emit_goto(iif.clone());
                self.label_here(if_true.clone());
                for stmt in ift.iter() {
                    self.stmt(stmt);
                }
                self.emit_goto(end.clone());

                self.label_here(if_false.clone());
                for stmt in iff.iter() {
                    self.stmt(stmt);
                }
                self.emit_goto(end.clone());

                let l = self.labels.clone();
                self.label_here(iif);
                self.emit(Opcode::JUMP_IF_TRUE(l.get(&if_true).unwrap().unwrap()));
                self.emit(Opcode::JUMP(l.get(&if_false).unwrap().unwrap()));
                self.label_here(end);



            }

            Stmt::While(cond,stmts) => {


                let while_start = self.new_empty_label();
                let while_end = self.new_empty_label();
                let while_block = self.new_empty_label();

                self.emit_goto(while_start.clone());
                self.label_here(while_block.clone());
                for stmt in stmts.iter() {
                    self.stmt(stmt);
                }

                self.label_here(while_start.clone());
                self.expr(cond.clone());
                let l = self.labels.clone();
                self.emit(Opcode::JUMP_IF_TRUE(l.get(&while_block.clone()).unwrap().unwrap()));
            }

            v => panic!("unimplemented {:?}",v),
        }
    }


    fn expr(&mut self,e: Expr) {
        match e {
            Expr::Get(arrname,id) => {
                self.expr(*id);
                self.expr(*arrname);

                self.emit(Opcode::SUBCR);

            }
            Expr::BooleanLiteral(b) => {
                if b {
                    self.emit(Opcode::PUSH(Value::Bool(N1::new(1))));
                } else {
                    self.emit(Opcode::PUSH(Value::Bool(N1::new(0))));
                }
            },
            Expr::Array(exprs) => {
                for e in exprs.iter() {
                    self.expr(e.clone());
                }

                self.emit(Opcode::BUILD_LIST(exprs.len()));
            }
            Expr::FloatLiteral(f) => {
                self.emit(Opcode::PUSH(Value::Float(f)));
            },
            Expr::IntLiteral(i) => self.emit(Opcode::PUSH(Value::Int(i))),

            Expr::Assign(ref varname,ref expr) => {
                self.expr(expr.as_ref().clone());
                self.emit(Opcode::STORE_FAST(varname.clone()));
            }
            Expr::StringLiteral(ref s) => self.emit(Opcode::PUSH(Value::String(s.clone()))),
            Expr::Nil => self.emit(Opcode::PUSH(Value::Null)),
            Expr::Identifier(ref iname) => {
                self.emit(Opcode::LOAD_FAST(iname.clone()));
            }
            Expr::Function(args,stmts) => {

                let mut cmpl = Compiler::new();


                for stmt in stmts.iter() {
                    cmpl.stmt(stmt);
                }
                let code = cmpl.finalize();
                let is_debug_on = unsafe {
                    if DEBUG == true {
                        true
                    } else {
                        false
                    }
                };
                if is_debug_on {
                    println!("{:?}",code.clone());
                }
                let mut fun = Value::Fun(code.clone(),args);
                self.emit(Opcode::PUSH(fun.clone()));
            }
            Expr::Call(target,args) => {
                let mut reversed_args = args.clone();
                reversed_args.reverse();
                for arg in reversed_args.iter() {
                    self.expr(arg.clone());
                }
                self.expr(*target);
                self.emit(Opcode::CALL(args.len()));
            }
            Expr::BinaryOp(op,lit1,lit2) => {
                match op {
                    BinaryOp::Add => {
                        self.expr(*lit1);
                        self.expr(*lit2);
                        self.emit(Opcode::ADD);
                    }
                    BinaryOp::Subtract => {
                        self.expr(*lit1);
                        self.expr(*lit2);
                        self.emit(Opcode::SUB);
                    }
                    BinaryOp::Multiply => {
                        self.expr(*lit1);
                        self.expr(*lit2);
                        self.emit(Opcode::MUL);
                    },
                    BinaryOp::Divide => {
                        self.expr(*lit1);
                        self.expr(*lit2);
                        self.emit(Opcode::DIV);
                    }
                    BinaryOp::GreaterThan => {
                        self.expr(*lit2);
                        self.expr(*lit1);
                        self.emit(Opcode::GT);
                    }
                    BinaryOp::GreaterEquals => {
                        self.expr(*lit2);
                        self.expr(*lit1);
                        self.emit(Opcode::GE);
                    }
                    BinaryOp::LessEquals => {
                        self.expr(*lit2);
                        self.expr(*lit1);
                        self.emit(Opcode::LE);
                    }
                    BinaryOp::LessThan => {
                        self.expr(*lit2);
                        self.expr(*lit1);
                        self.emit(Opcode::LT);
                    }
                    BinaryOp::Equals => {
                        self.expr(*lit2);
                        self.expr(*lit1);
                        self.emit(Opcode::EQ);
                    }
                    v => panic!("unimplemented {:?}",v),
                }
            }
            Expr::UnaryOp(op,lit) => {
                match op {
                    UnaryOp::UnaryMinus => {
                        self.expr(*lit);
                        self.emit(Opcode::UnaryMinus);
                    }
                    UnaryOp::Not => {
                        self.expr(*lit);
                        self.emit(Opcode::Not);
                    }
                }
            }



        }
    }
}